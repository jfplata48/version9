from django.apps import AppConfig


class SonajeroappConfig(AppConfig):
    name = 'sonajeroapp'
