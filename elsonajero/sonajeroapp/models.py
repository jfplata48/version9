from django.db import models

# Create your models here.


class Pais(models.Model):
	Nombre = models.CharField(max_length = 80, blank = False, null=False)
	def __str__(self):
		self.Nombre

class Departamento(models.Model):
	Nombre = models.CharField(max_length = 80, blank = False, null=False)
	NombrePais= models.ForeignKey(Pais, blank=True, null=True)
	def __init__(self):
		self.Nombre

class Categorias(models.Model):
	Nombre = models.CharField(max_length = 80, blank = False, null=False)
	def __init__(self):
		self.Nombre

class TipoContenido(models.Model):
	Nombre = models.CharField(max_length = 80, blank = False, null=False)
	def __init__(self):
		self.Nombre

class OrdenDestacado(models.Model):
	Nombre = models.CharField(max_length = 80, blank = False, null=False)
	def __init__(self):
		self.Nombre

class TipoDestacado(models.Model):
	Nombre = models.CharField(max_length = 80, blank = False, null=False)
	def __init__(self):
		self.Nombre 

class Tags(models.Model):
	Nombre = models.CharField(max_length = 80, blank = False, null=False)
	def __init__(self):	
		self.Nombre

class EstadoNoticia(models.Model):
	Nombre = models.CharField(max_length = 80, blank = False, null=False)
	def __init__(self):
		self.Nombre
		
class Noticia_Hoy(models.Model):
	Descripcion = models.CharField(max_length = 350, blank = False, null=False)
	Fuente = models.CharField(max_length = 60, blank = False, null=False) 
	TipoContenido = models.ForeignKey(TipoContenido, blank = False, null=False)
	Audio = models.CharField(max_length = 100, blank = True, null=True)
	Video = models.CharField(max_length = 100, blank = True, null=True)
	Texto = models.CharField(max_length = 10000, blank = False, null=False)
	HTML = models.CharField(max_length = 10000, blank = True, null=True) 
	Categorias = models.ForeignKey(Categorias, blank = False, null=False)
	EstadoNoticia = models.ForeignKey(EstadoNoticia, blank = False, null=False)
	TipoDestacado = models.ForeignKey(TipoDestacado, blank = False, null=False)
	OrdenDestacado = models.ForeignKey(OrdenDestacado, blank = False, null=False)
	Pais = models.ForeignKey(Pais, blank = False, null=False)
	Departamento = models.ForeignKey(Departamento, blank = False, null=False)
	VimeoID = models.CharField(max_length = 10000, blank = True, null=True) 
	FechaInicio = models.DateTimeField('Fecha de inicio de publicación', blank=True, null=True)
	FechaFin = models.DateTimeField('Fecha Final', blank=True, null=True)
	Tags = models.ForeignKey(Tags, blank = False, null=False)

